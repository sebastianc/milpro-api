$( document ).ready(function() {
	"use strict";

    //Security Token
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	
	// Detect OS
	if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('LINUX X86_64') >= 0 ) { 
		  $("body").addClass("mac");
		} else {
		  $("body").addClass("pc");
		}
		
	// Pop up windows
	$(".close").click(function(){
		 $(this).parent().toggleClass("hidden");
		 $(".box-overlay").toggleClass("hidden");
         $('body').css('overflow', 'auto'); 
		});	 
	$(".box-overlay").click(function(){		 
		 $(this).toggleClass("hidden");
		 $(".pop-up").addClass("hidden");
         $('body').css('overflow', 'auto'); 
		});	 
	$(".pop-up-link").click(function(){		 
		$(".pop-up").each(function() {
		   $(this).addClass("hidden");
           $('body').css('overflow', 'hidden'); 
		});
        $(".box-overlay").addClass("hidden");
        var id = $(this).attr("id");
        id = id.replace("-link","");
        if(id.indexOf("editCompany") >= 0){ id = "editCompany";}
        $(".box-overlay").toggleClass("hidden");
        $("#"+id).toggleClass("hidden");

	});	
	
	//Datepicker
	if($('.datepicker')) {
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true
		});
		$("#datepicker").on("change", function () {
			$("#date").val($("#datepicker").val());
		});

        $("#datepickerNew").on("change", function() {
            $("#dateNew").val($("#datepickerNew").val());
        });
	}

	//Notification message hiding
	if($('.hide-after-delay')) {
		$('.hide-after-delay').fadeIn(450).delay(4000).fadeOut(900);
	}

    //Notify the user on the status
    function createStatusMessage(message, success, id) {
        var className = success ? 'success_message' : 'error_message';
        var alert = $('<div></div>').text(message).addClass(className);
        $('#product').prepend(alert);
        $('#product').find('#'+id).remove();
        alert.fadeIn(450).delay(4000).fadeOut(900)
    }

	//Removing products
    if($('.remove-prod')) {
        $(".remove-prod").on("click",function(){ // When btn is pressed.
            var url = $(this).data('prod-url');
            var id = $(this).data('prod-id');
            //$.post(BASE_URL + '/' + Id);
            $.post(url);
            if($('#product')) {
                createStatusMessage('Product removed.', true, id);
            }
        });
    }

    //Removing agents
    if($('.remove-agen')) {
        $(".remove-agen").on("click",function(){ // When btn is pressed.

            if (confirm('Are you sure you want to delete this user?')) {
                var url = $(this).data('agent-url');
                var id = $(this).data('agent-id');
                $.post(url);
                var alert = $('<div></div>').text('Agent removed.').addClass('success_message');
                $('body').prepend(alert);
                $('body').find('#agent'+id).remove();
                alert.fadeIn(450).delay(4000).fadeOut(900);
                setTimeout(
                    function()
                    {
                        location.reload();
                    }, 5000);
            } else {

            }

        });
    }

    if($('.edit-comp')) {
        $(".edit-comp").on("click",function(){ // When btn is pressed.
            var name = $(this).data('comp-name');
            var id = $(this).data('comp-id');
            var email = $(this).data('comp-email');
            var accts = $(this).data('comp-accts');
            var expire = $(this).data('comp-expire');
            $("#date").val(expire);
            $("#datepicker").attr('placeholder','Expiry Date '+expire);
            $("#editCompanyForm").attr('action','/dashboard/companies/edit/'+id);
            $("#datepicker").val(expire);
            $("#companyAccts").attr('placeholder',accts);
            $("#companyAccts").val(accts);
            $("#companyEmail").attr('placeholder',email);
            $("#companyEmail").val(email);
            $("#companyName").attr('placeholder',name);
            $("#companyName").val(name);
            $(".accmsg").text('Currently there are '+accts+' assigned accounts');

        });
    }
});
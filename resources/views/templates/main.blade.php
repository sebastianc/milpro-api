<!doctype html>
<html lang="en">

    <head>
        <title>@yield('title')MilPro</title>

        <link rel="shortcut icon" href="{{ url('favicon.ico') }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{--Meta OG tags--}}
        <meta property="og:site_name" content="MilPro">
        @yield('meta')

        {{-- TODO: Remove no follow --}}
        <meta NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

        @if (Request::is('/'))
            {{-- TODO: UPDATE META --}}
            <meta name="description" content="MilPro">
            <meta name="keywords" content="MilPro">
        @endif

        @include('assets.styles')

    </head>

    <body>

        <!-- #masthead -->
        <!-- Popups -->
        <div class="box-overlay hidden">
        </div>

        @include('popups.errors')
        {{--@include('popups.login')--}}
        @stack('account-popups')
        @stack('company-popups')

        @yield('popups')
        {{--@yield('popover')--}}

        @yield('content')

        @include('templates.footer')
        @include('assets.scripts')
        {{--@yield('footer_scripts') --}}

    </body>

</html>

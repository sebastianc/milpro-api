@extends('templates.main')

@section('title') {{ $agent->name }} Product Percentages - @stop

@section('body_class') class="product_percentages_page" @stop

@section('content')
    @include('partials.percentages-table')
@stop

@section('footer')
    @include('partials.footer')
@stop
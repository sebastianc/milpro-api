@extends('templates.main')

@section('title') Log In - @stop

@section('body_class') class="white-bg" @stop

@section('content')
    @include('partials.login-form')
@stop
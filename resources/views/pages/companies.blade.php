@extends('templates.main')

@section('title') Companies - @stop

@section('body_class') class="companies_page" @stop

@section('content')
    @include('partials.companies-table')
@stop

@section('footer')
    @include('partials.footer')
@stop
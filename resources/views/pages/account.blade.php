@extends('templates.main')

@section('title') {{ $user->name }} Accounts - @stop

@section('body_class') class="accounts_page" @stop

@section('content')
    @include('partials.accounts-table')
@stop

@section('footer')
    @include('partials.footer')
@stop
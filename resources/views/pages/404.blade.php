@extends('templates.main')

@section('title') 404 - @stop

@section('body_class') class="404_page" @stop

@section('content')
    @include('partials.404')
@stop
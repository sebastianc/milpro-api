<div id="editCompany" class="pop-up hidden"> <a class="close pull-right"><i class="fa fa-times" aria-hidden="true"></i></a>
    <article class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 margin-bottom-5">
        <h3 class="green text-capitalize text-center col-sm-12">Edit company</h3>
        <form id="editCompanyForm" action="/dashboard/companies/edit/" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="form-group">
                <input type="text" class="form-control" name="name" id="companyName" value="">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" name="email" id="companyEmail" value="">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="accts_nr" id="companyAccts" value="">
            </div>
            <p class="col-sm-12 "><small class="light-grey accmsg">Currently there are X assigned accounts</small></p>
            <div class="form-group">
                <input id="datepicker" type="type" class="form-control datepicker" placeholder="Expiry Date " name="datepicker">
                <input type="hidden" name="premium_expire" id="date" value="">
            </div>
        </form>
    </article>
    <a class="btn green-bg col-xs-12 white" onClick="document.getElementById('editCompanyForm').submit();">Save</a>
</div>
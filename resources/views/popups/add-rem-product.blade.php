<div id="product" class="pop-up hidden"> <a class="close pull-right"><i class="fa fa-times" aria-hidden="true"></i></a>
    <h3 class="green text-capitalize text-center">Add Product</h3>
    <article class="text-holder margin-bottom-2">
        <form id="productForm" class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1" action="/dashboard/account/add-prod" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Product Name">
            </div>
        </form>
    </article>
    <article class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 no-padding margin-bottom-5">
        <h4 class="text-left dark-grey">Product List</h4>
        @foreach($products as $key => $product)
            <div id="{{ $product->id }}" class="col-xs-12 no-padding">
                <div class="col-xs-9 no-padding">
                    <p class="white-bg product-holder"><img src="/assets/images/product-list-icon.png" alt="" class="pull-left"> {{ $product->name }} <small class="white green-bg pull-right rounded">{{ $key+1 }}</small></p>
                </div>
                <a data-prod-url="/dashboard/account/remove-prod/{{ $product->id }}" data-prod-id="{{ $product->id }}" class="col-xs-3 white light-grey-bg btn text-center remove-prod">Remove</a>
            </div>
        @endforeach
    </article>
    <a class="btn green-bg col-xs-12 white" onClick="document.getElementById('productForm').submit();" >Save</a>
</div>
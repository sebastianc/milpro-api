<div id="addNewCompany" class="pop-up hidden"> <a class="close pull-right"><i class="fa fa-times" aria-hidden="true"></i></a>
    <article class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 margin-bottom-5">
        <h3 class="green text-capitalize text-center col-sm-12">Add company</h3>
        <form id="addNewCompanyForm" action="/dashboard/companies/add" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="premium" value="Y">
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Company Name">
            </div>
            <div class="form-group">
                <input type="text" name="accts_nr" class="form-control" placeholder="Add Accounts">
            </div>
            <div class="form-group">
                <input id="datepickerNew" type="type" class="form-control datepicker" placeholder="Expiry Date" name="datepicker">
                <input type="hidden" name="premium_expire" id="dateNew" value="">
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email Address">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
        </form>
    </article>
    <a class="btn green-bg col-xs-12 white" onClick="document.getElementById('addNewCompanyForm').submit();">Save</a>
</div>
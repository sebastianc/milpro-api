<div id="register" class="pop-up hidden white-bg " >
    <a class="close pull-right"></a>
    <div class="col-xs-12 white-bg">
        <div class="col-sm-12 text-center">
            <img src="{{asset('assets/images/SOLE.png')}}"/></div>
        <div id="register-form" class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3 margin_bottom_1">
            <h3 class="col-xs-12 text-center text-capitalize post-title dark-grey">Register</h3>
            <div class="form_wrap col-sm-12 no-padding">
                <form name="login" id="" action="" method="">
                        <div class="form-group">
                            <input class="form-control cl-form-control" type="text" name="username" id="username" placeholder="Userame" >
                        </div>
                        <div class="form-group">
                            <input class="form-control cl-form-control" type="email" name="email" id="email" placeholder="Email" >
                        </div>
                        <div class="form-group">
                            <input class="form-control cl-form-control" type="password" name="password" id="password" placeholder="Password" >
                        </div>
                    </fieldset>
                    <a id="register_btn"  class="btn sole-btn red-btn col-xs-12"  >Register <div class="register_loader"><img src="{{asset('assets/images/rolling.svg')}}"></div></a>
                </form>
            </div>

            <div class=" col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3">
                <span class="col-xs-12 text-center margin-bottom-1 margin-top-1">or</span>
            </div>
            <div class="col-xs-12 no-padding">
                <a id="login-link" class="col-xs-12 text-center no-padding pop-up-link dark-grey">Already have an account? <b>Login</b></a>
            </div>
        </div>
        <!-- -->
        <div id="upload-pic-form" class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-3 margin-bottom-1 ">
{{--            {{ Form::open( array('url' => '/upload-image', 'name'=>'upload_img_form', 'files' => true)) }}--}}
            <form action="/upload-image" method="POST" name="upload_img_form" enctype="multipart/form-data">
            <div class="form_wrap">
                <h4 class="col-sm-12  margin-bottom-1 text-center">Add photo</h4>
                <div class="col-sm-12 col-xs-12 text-center margin-bottom-1">
                    {{--<form action="/file-upload" class="dropzone profile_img"></form>--}}
                    <div class="profile_img" id="preview_upload" style="background-image: url({{asset('assets/images/camera.png')}})"></div>
                    <input type="file" name="image" id="upload_profile_image">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                </div>
            </div>
            <input name="submit" type="submit" id="save-pic"  class="btn sole-btn red-btn col-xs-12 margin-bottom-1" value="Done" disabled/>
            <div class="col-xs-12 text-center"><a href="/account?account_created=" class="grey-font">Skip</a></div>
            </form>
        </div>
    </div>
</div>


<div id="addNewUser" class="pop-up hidden"> <a class="close pull-right"><i class="fa fa-times" aria-hidden="true"></i></a>
    <article class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 margin-bottom-5">
        <h3 class="green text-capitalize text-center col-sm-12">Add new user</h3>
        <form id="addNewUserForm" action="/dashboard/account/register" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Agent Name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="area" placeholder="Area">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="email" placeholder="Email Address">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
            <small class="col-sm-12 light-grey">{{ $user->accounts_left }} account(s) left to assign</small>
        </form>
    </article>
    <a class="btn green-bg col-xs-12 white" onClick="document.getElementById('addNewUserForm').submit();" >Save</a>
</div>
<div id="login" class="pop-up hidden white-bg">
    <a class="close pull-right"></a>
    @include('partials.login-form')
</div>
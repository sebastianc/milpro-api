
<div class="error_message{{ Session::has('error_message') ? ' hide-after-delay' : '' }}">{{ Session::pull('error_message') }}</div>
<div class="success_message{{ Session::has('success_message') ? ' hide-after-delay' : '' }}">{{ Session::pull('success_message')  }}</div>

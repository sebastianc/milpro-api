<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <title>MilPro Reset</title>

    <style type="text/css">

        div, p, a, li, td { -webkit-text-size-adjust:none; }

        *{
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .ReadMsgBody
        {width: 100%; background-color: #ffffff;}
        .ExternalClass
        {width: 100%; background-color: #ffffff;}
        body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
        html{width: 100%; background-color: #ffffff;}

        @font-face {font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

        @font-face {font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

        @font-face {font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

        @font-face {font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

        @font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

        @font-face {font-family: 'proxima_novaextrabold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff2') format('woff2'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}


        p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }

        .hover:hover {opacity:0.85;filter:alpha(opacity=85);}

        .image73 img {width: 73px; height: auto;}
        .image42 img {width: 42px; height: auto;}
        .image400 img {width: 400px; height: auto;}
        .icon49 img {width: 49px; height: auto;}
        .image113 img {width: 113px; height: auto;}
        .image70 img {width: 70px; height: auto;}
        .image67 img {width: 67px; height: auto;}
        .image80 img {width: 80px; height: auto;}
        .image35 img {width: 35px; height: auto;}
        .icon49 img {width: 49px; height: auto;}

    </style>



    <style type="text/css"> @media only screen and (max-width: 479px){
            body{width:auto!important;}
            table[class=full] {width: 100%!important; clear: both; }
            table[class=mobile] {width: 100%!important; padding-left: 30px; padding-right: 30px; clear: both; }
            table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
            td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
            *[class=erase] {display: none;}
            *[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
            .image400 img {width: 100%!important; height: auto;}
        }
        } </style>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">

<div class="ui-sortable" id="sort_them">
    <!-- Notification 1  -->
    <div style="display: none;" id="element_0036193629959598184"></div><!-- End Notification 1 -->

    <!-- Notification 2  -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" >
        <tr>
            <td align="center" style="background-image: url('http://rocketway.net/themebuilder/products/notifications/patterns/arab_tile.png'); -webkit-background-size: inherit; background-position: initial initial; background-repeat: repeat repeat;" id="not2ChangeBG" bgcolor="#001620"oldcss="background-image: url('http://rocketway.net/themebuilder/products/notifications/patterns/corrugation.png'); -webkit-background-size: inherit; background-position: initial initial; background-repeat: repeat repeat;">


                <!-- Mobile Wrapper -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                    <tr>
                        <td width="100%" height="100" align="center">

                            <!-- SORTABLE -->
                            <div class="sortable_inner ui-sortable">

                                <!-- Space -->
                                <table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                                    <tr>
                                        <td width="100%" height="50"></td>
                                    </tr>
                                </table><table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#fe5258"object="drag-module-small" style="border-top-right-radius: 6px; border-top-left-radius: 6px; background-color: rgb(38, 38, 38); position: relative; z-index: 0;">
                                    <tr>
                                        <td width="100%" valign="middle" align="center">

                                            <!-- Header Text -->
                                            <table width="280" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                <tr>
                                                    <td width="100%" height="35"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" width="100%" style="text-align: center; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 35px; color: #ffffff; line-height: 40px; font-weight: 100;" class="fullCenter"  cu-identify="element_08686626967974007"><div style="text-align: center;"><br></div><div style="text-align: center;"><img src="https://cdn-sgqmu6ewk.netdna-ssl.com/wp-content/uploads/Dreamr-Logo-Manchester-Retina-1.png" alt="MilPro Logo" width="50" height="78" class="alignleft size-full wp-image-7643"></div><div style="text-align: center;">Here you go!</div></td>
                                                </tr>
                                                <tr>
                                                    <td width="100%" height="30" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </table><!-- End Space -->

                                <!-- Space -->
                                <div style="display: none" id="element_06729999182280153"></div><!-- End Space -->

                                <!-- Start Top -->


                                <table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#fe5258"object="drag-module-small" style="background-color: rgb(38, 38, 38);">
                                    <tr>
                                        <td width="100%" valign="middle" align="center">

                                            <table width="400" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                <tr>
                                                    <td width="400"  class="image400" style="line-height: 1px;">
                                                        <a href="#" style="text-decoration: none;" cu-identify="element_016894123377278447"><img src="https://cdn-sgqmu6ewk.netdna-ssl.com/wp-content/uploads/Dreamr-Logo-Manchester-Retina-1.png" width="380" alt="" border="0" class="hover"  cu-identify="element_09220392170827836" id="element_05964746717363596" test="test" style="display: none; width: 0px; height: 0px;"></a>
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </table>

                                <table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                    <tr>
                                        <td width="100%" valign="middle" align="center">

                                            <table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                <tr>
                                                    <td width="100%" height="40"></td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </table>

                                <div style="display: none;" id="element_009291995340026915"></div>

                                <table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                                    <tr>
                                        <td width="100%" valign="middle" align="center">

                                            <table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                <tr>
                                                    <td valign="middle" width="100%" style="text-align: center; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 14px; color: #8e9197; line-height: 24px; font-weight: 400;" class="fullCenter"  cu-identify="element_041617606044746935"><span style="color: rgb(44, 45, 48); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; background-color: rgb(249, 249, 249);">Hi {{$data['user']['name']}},&nbsp;</span><div><span style="color: rgb(44, 45, 48); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; background-color: rgb(249, 249, 249);"><br></span></div><div><span style="color: rgb(44, 45, 48); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; background-color: rgb(249, 249, 249);">You recently requested a new password.&nbsp; Here it is: <br> New Password = {{$data['password']}}</span></div><div><span style="color: rgb(44, 45, 48); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; background-color: rgb(249, 249, 249);"><br></span></div><div><span style="color: rgb(44, 45, 48); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; background-color: rgb(249, 249, 249);">If you did not request this please submit a support request. </span></div><div><span style="color: rgb(44, 45, 48); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; background-color: rgb(249, 249, 249);"><br></span></div><div><span style="color: rgb(44, 45, 48); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; background-color: rgb(249, 249, 249);">&nbsp;</span></div><div><span style="color: rgb(44, 45, 48); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; background-color: rgb(249, 249, 249);"><br></span></div><div><span style="color: rgb(44, 45, 48); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; background-color: rgb(249, 249, 249);">MilPro</span></div></td>
                                                </tr>
                                                <tr>
                                                    <td width="100%" height="35" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </table>

                                <div style="display: none;" id="element_06704901691991836"></div>

                                <div style="display: none;" id="element_0001262502744793892"></div>

                                <div style="display: none;" id="element_08145668897777796"></div>

                                <!-- CopyRight -->
                                <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small" cu-identifier="element_08090022606775165" style="position: relative; z-index: 0;">
                                    <tr>
                                        <td width="100%" height="25"></td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" width="100%" style="text-align: center; font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #8e9197; font-size: 12px; font-weight: 400; line-height: 18px;" class="fullCenter"  cu-identify="element_07534935888834298"><div style="text-align: center;"><a href="http://milpro.dreamrserve.uk" style="font-family: Verdana; font-size: x-small;">milpro.dreamrserve.uk</a></div><div style="text-align: center;"><span style="font-family: Verdana; font-size: x-small;">Copyright © 2016 MilPro | UK Trainer Releases &amp; News | Company Reg No. 00000</span></div></td>
                                    </tr>
                                    <tr>
                                        <td width="100%" height="20" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" width="100%" style="text-align: center; font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #fe5258; font-size: 12px; font-weight: 400; line-height: 18px;" class="fullCenter">
                                            <!--subscribe--><a href="http://milpro.dreamrserve.uk" style="text-decoration: none; color: #fe5258" cu-identify="element_06280266107060015">Unsubscribe</a><!--unsub-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" height="60" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="100%" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                    </tr>
                                </table><!-- End CopyRight -->

                            </div>

                        </td>
                    </tr>
                </table>

</div>
</td>
</tr>
</table><!-- End Notification 2 -->

<!-- Notification 3  -->
<div style="display: none;" id="element_04326089497189969"></div><!-- End Notification 3 -->

<!-- Notification 4  -->
<div style="display: none;" id="element_0047121889190748334"></div><!-- End Notification 4 -->

<!-- Notification 5  -->
<div style="display: none;" id="element_016324668540619314"></div><!-- End Notification 5 -->

<!-- Notification 6  -->
<div style="display: none;" id="element_08386508214753121"></div><!-- End Notification 6 -->


</div>
</body>	<style>body{ background: url('http://rocketway.net/themebuilder/products/notifications/patterns/cracks_1.png) !important; } </style>
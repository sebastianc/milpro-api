<section class="login">
    <article class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 text-center">
        <div class="img-holder">
            <img src="/assets/images/logo_dark.png" alt="" class="img-responsive">
        </div>
        <h4 class="dark-grey">Improving sales <br/> performance</h4>
        <h3 class="green text-capitalize text-center col-sm-12">Log in</h3>
        <form name="login" id="login" action="/dashboard/login" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">

            <div class="form-group">
                <input type="email" name="email" class="form-control" value="" placeholder="Email Address">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" value="" placeholder="Password">
            </div>
            <input type="submit" name="submit" id=""  class="btn green-bg col-xs-12 white" value="Log in" />
        </form>
    </article>
</section>
<footer role="contentinfo" class="container-fluid footer"> <small class="pull-right">Copyright &copy;
  <time datetime="{{date('Y')}}">{{date('Y')}}</time>
  </small>
</footer>
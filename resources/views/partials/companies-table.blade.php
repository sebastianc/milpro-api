@push('company-popups')
    @include('popups.add-company')
    @include('popups.edit-company')
@endpush

<header role="banner" class="container-fluid dark-grey-bg">
    <nav role="navigation" class="navbar"> <a class="navbar-brand" id="owner" onMouseOver="document.getElementById('owner').style.cursor='default';document.getElementById('imgowner').style.cursor='default'"><img id="imgowner" src="/assets/images/logo.png" alt="" class="img-responsive"></a>
        <ul class="nav navbar-nav pull-right white">
            <li class="nav-item active"> {{ $user->name }} </li>
            <li class="nav-item"> <a class="btn light-grey white-bg " href="/dashboard/logout">Logout</a> </li>
        </ul>
    </nav>
</header>

<div class="container-fluid white-bg dropdown-holder">
    <section class="col-sm-12"><div class="col-sm-12"><a id="addNewCompany-link" class="btn green-bg pop-up-link white pull-right">Add New Company</a></div></section>
</div>

<div class="container-fluid">
    <main role="main" class="main-wrapper">
        <section class="col-sm-12">
            <article>
                <table class="table table-striped">
                    <thead class="light-grey-bg">
                    <tr>
                        <th class="white">Company</th>
                        <th class="white">Date Created</th>
                        <th class="white">Expiry Date</th>
                        <th class="white">No Accounts Assigned</th>
                        <th width="40%"></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($companies as $key => $company)
                            @if($key%2)
                                <tr class="even">
                                    <td>
                                        {{ $company['name'] }}
                                    </td>
                                    <td>
                                        {{ date('d/m/Y',strtotime($company['created_at'])) }}
                                    </td>
                                    <td>
                                        {{ date('d/m/Y',strtotime($company['premium_expire'])) }}
                                    </td>
                                    <td>
                                        {{$company['used_acc']}}/{{ $company['accts_nr'] }}
                                    </td>
                                    <td>
                                        <a id="editCompany-link-{{ $company['id'] }}" class="btn light-grey-bg white pull-right pop-up-link edit-comp" data-comp-id="{{ $company['id'] }}" data-comp-expire="{{ date('Y-m-d',strtotime($company['premium_expire'])) }}" data-comp-accts="{{ $company['accts_nr'] }}" data-comp-email="{{ $company['email'] }}" data-comp-name="{{ $company['name'] }}">Edit</a>
                                    </td>
                                </tr>
                            @else
                                <tr class="odd">
                                    <td>
                                        {{ $company['name'] }}
                                    </td>
                                    <td>
                                        {{ date('d/m/Y',strtotime($company['created_at'])) }}
                                    </td>
                                    <td>
                                        {{ date('d/m/Y',strtotime($company['premium_expire'])) }}
                                    </td>
                                    <td>
                                        {{$company['used_acc']}}/{{ $company['accts_nr'] }}
                                    </td>
                                    <td>
                                        <a id="editCompany-link-{{ $company['id'] }}" class="btn light-grey-bg white pull-right pop-up-link edit-comp" data-comp-id="{{ $company['id'] }}" data-comp-expire="{{ date('Y-m-d',strtotime($company['premium_expire'])) }}" data-comp-accts="{{ $company['accts_nr'] }}" data-comp-email="{{ $company['email'] }}" data-comp-name="{{ $company['name'] }}">Edit</a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </article>
        </section>
    </main>
</div>
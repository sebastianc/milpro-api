@push('account-popups')
    @include('popups.add-user')
    @include('popups.add-rem-product')
@endpush
<header role="banner" class="container-fluid dark-grey-bg">
    <nav role="navigation" class="navbar"> <a class="navbar-brand" href="/dashboard/account/{{ date('m/Y',strtotime(date('d-m-Y'))) }}"><img src="/assets/images/logo.png" alt="" class="img-responsive"></a>
        <ul class="nav navbar-nav pull-right white">
            <li class="nav-item active"> {{ $user->name }} </li>
            <li class="nav-item"> <a class="btn light-grey white-bg " href="/dashboard/logout">Logout</a> </li>
        </ul>
    </nav>
</header>

<div class="container-fluid white-bg dropdown-holder">
    <div class="dropdown col-sm-12">
        <a class="btn white-bg light-grey dropdown-toggle" href="/dashboard/account/{{ date('m/Y',strtotime('01-'.$month.'-'.$year)) }}" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ date('F Y',strtotime('01-'.$month.'-'.$year)) }} <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @php
            $start    = (new DateTime($user->created_at))->modify('first day of this month');
            $end      = (new DateTime(date("Y-m-d")))->modify('first day of next month');
            $interval = DateInterval::createFromDateString('1 month');
            $period   = new DatePeriod($start, $interval, $end);
            @endphp
            @foreach($period as $dt)
                <a class="dropdown-item" href="/dashboard/account/{{$dt->format("m/Y")}}">{{$dt->format("F Y")}}</a>
            @endforeach
        </div>
    </div>
</div>

<div class="container-fluid">
    <main role="main">
        <section class="col-sm-12">
            <article>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="white no-padding" width="10.3%" colspan="2"><header class="green-bg">
                                    <h2 class="white text-center">Year-to-date Sales</h2>
                                </header></th>
                            <th width="30px" class="grey-bg"></th>
                            <th class="white no-padding" width="7.64%" colspan="2"><header class="green-bg">
                                    <h2 class="white text-center">Performance</h2>
                                </header></th>
                            <th width="30px" class="grey-bg"></th>
                            <th class="white no-padding" width="7.64%" colspan="2"><header class="green-bg">
                                    <h2 class="white text-center">Today's Performance</h2>
                                </header></th>
                            <th width="30px" class="grey-bg"></th>
                            <th class="white no-padding" colspan="5"><header class="green-bg">
                                    <h2 class="white text-center">Year-to-date</h2>
                                </header></th>
                        </tr>
                        <tr height="25px" class="grey-bg"></tr>
                        <tr class="light-grey-bg">
                            <th class="white" width="10.3%">Area</th>
                            <th class="white" width="13.5%">Name</th>
                            <th width="30px" class="grey-bg"></th>
                            <th class="white" width="7.64%">Actual %</th>
                            <th class="white" width="7.64%">YTD <br/>
                                Average</th>
                            <th width="30px" class="grey-bg"></th>
                            <th class="white" width="7.64%">Orders</th>
                            <th class="white" width="7.64%">Enquiry</th>
                            <th width="30px" class="grey-bg"></th>
                            <th class="white">Month Orders<br/>
                                Achieved</th>
                            <th class="white">Cum Orders<br/>
                                Achieved</th>
                            <th class="white">Month<br/>
                                Enquiry</th>
                            <th class="white">Cum <br/>
                                Enquiry</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($agents as $key => $agent)
                            @if($key%2)
                                <tr class="even" id="agent{{ $agent['id'] }}">
                                    <td>
                                        {{ $agent['area'] }}
                                    </td>
                                    <td>
                                        <a href="/dashboard/account/percentages/{{ $agent['id'] }}">{{ $agent['name'] }}</a>
                                    </td>
                                    <td class="grey-bg"></td>
                                    <td>
                                        {{ $agent['actual'] }}
                                    </td>
                                    <td>
                                        {{ $agent['ytd_avg'] }}
                                    </td>
                                    <td class="grey-bg"></td>
                                    <td>
                                        {{ $agent['orders'] }}
                                    </td>
                                    <td>
                                        {{ $agent['enquiry'] }}
                                    </td>
                                    <td class="grey-bg"></td>
                                    <td>
                                        {{ $agent['month_orders'] }}
                                    </td>
                                    <td>
                                        {{ $agent['cum_orders'] }}
                                    </td>
                                    <td>
                                        {{ $agent['month_enquiry'] }}
                                    </td>
                                    <td>
                                        {{ $agent['cum_enquiry'] }}
                                    </td>
                                    <td>
                                        <a class="btn light-grey-bg white pull-right remove-agen" data-agent-url="/dashboard/account/remove/{{ $agent['id'] }}" data-agent-id="{{ $agent['id'] }}">Remove</a>
                                    </td>
                                </tr>
                            @else
                                <tr class="odd">
                                    <td>
                                        {{ $agent['area'] }}
                                    </td>
                                    <td>
                                        <a href="/dashboard/account/percentages/{{ $agent['id'] }}">{{ $agent['name'] }}</a>
                                    </td>
                                    <td class="grey-bg"></td>
                                    <td>
                                        {{ $agent['actual'] }}
                                    </td>
                                    <td>
                                        {{ $agent['ytd_avg'] }}
                                    </td>
                                    <td class="grey-bg"></td>
                                    <td>
                                        {{ $agent['orders'] }}
                                    </td>
                                    <td>
                                        {{ $agent['enquiry'] }}
                                    </td>
                                    <td class="grey-bg"></td>
                                    <td>
                                        {{ $agent['month_orders'] }}
                                    </td>
                                    <td>
                                        {{ $agent['cum_orders'] }}
                                    </td>
                                    <td>
                                        {{ $agent['month_enquiry'] }}
                                    </td>
                                    <td>
                                        {{ $agent['cum_enquiry'] }}
                                    </td>
                                    <td>
                                        <a class="btn light-grey-bg white pull-right remove-agen" data-agent-url="/dashboard/account/remove/{{ $agent['id'] }}" data-agent-id="{{ $agent['id'] }}"">Remove</a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                    <tfoot class="no-padding-left">
                        <tr height="30px"></tr>
                        <tr>
                            <th class="white" colspan="2"> <div class="col-sm-5 no-padding"> <a id="addNewUser-link" class="btn green-bg pop-up-link white col-sm-12">Add New User</a> </div>
                                <div class="col-sm-7 no-padding-right"> <a id="product-link" class="btn green-bg pop-up-link white col-sm-12">Add/Remove Product</a> </div>
                            </th>
                            <th class="white"></th>
                            <th class="white"></th>
                            <th class="white"></th>
                            <th class="white"></th>
                            <th class="white"></th>
                            <th class="white"></th>
                            <th class="white"></th>
                            <th class="white"></th>
                            <th class="white"></th>
                            <th class="white"></th>
                        </tr>
                    </tfoot>
                </table>
            </article>
        </section>
    </main>
</div>
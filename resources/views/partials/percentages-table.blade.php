<header role="banner" class="container-fluid dark-grey-bg">
    <nav role="navigation" class="navbar"> <a class="navbar-brand" href="/dashboard/account/{{ date('m/Y',strtotime(date('d-m-Y'))) }}"><img src="/assets/images/logo.png" alt="" class="img-responsive"></a>
        <ul class="nav navbar-nav pull-right white">
            <li class="nav-item active"> {{ $user->name }} </li>
            <li class="nav-item"> <a class="btn light-grey white-bg " href="/dashboard/logout">Logout</a> </li>
        </ul>
    </nav>
</header>

<div class="container-fluid white-bg dropdown-holder">
    <div class="dropdown col-sm-12">
        <a class="btn white-bg light-grey dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $agent->name }} <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @foreach ($agents as $ag)
                <a class="dropdown-item" href="/dashboard/account/percentages/{{$ag->id}}">{{$ag->name}}</a>
            @endforeach
        </div>
    </div>
</div>

<div class="container-fluid">
    <main role="main">
        <section class="col-sm-12">
            <header class="green-bg">
                <h2 class="white text-center">Product Categories</h2>
            </header>
            <article>
                <table class="table table-striped">
                    <thead class="light-grey-bg">
                        <tr>
                            <th></th>
                            <th class="white">Product 1</th>
                            <th class="white">Product 2</th>
                            <th class="white">Product 3</th>
                            <th class="white">Product 4</th>
                            <th class="white">Product 5</th>
                            <th class="white">Product 6</th>
                            <th class="white">Product 7</th>
                            <th class="white">Product 8</th>
                            <th class="white">Product 9</th>
                            <th class="white">Product 10</th>
                            <th class="white">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($calls as $key => $call)
                            @if($key%2)
                                <tr class="even">
                                    <td>
                                    </td>
                                    <td>
                                        {{ $call['p1']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p2']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p3']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p4']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p5']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p6']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p7']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p8']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p9']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p10']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        100
                                    </td>
                                </tr>
                            @else
                                <tr class="odd">
                                    <td>
                                    </td>
                                    <td>
                                        {{ $call['p1']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p2']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p3']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p4']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p5']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p6']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p7']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p8']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p9']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        {{ $call['p10']*$call['percentage'] }}
                                    </td>
                                    <td>
                                        100
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                    <tfoot class="light-grey-bg">
                        <tr>
                            <th class="white">
                                Percentage<br/>Focus
                            </th>
                            <th class="white">
                                {{ $allPercentages['p1Percent'] }}%
                            </th>
                            <th class="white">
                                {{ $allPercentages['p2Percent'] }}%
                            </th>
                            <th class="white">
                                {{ $allPercentages['p3Percent'] }}%
                            </th>
                            <th class="white">
                                {{ $allPercentages['p4Percent'] }}%
                            </th>
                            <th class="white">
                                {{ $allPercentages['p5Percent'] }}%
                            </th>
                            <th class="white">
                                {{ $allPercentages['p6Percent'] }}%
                            </th>
                            <th class="white">
                                {{ $allPercentages['p7Percent'] }}%
                            </th>
                            <th class="white">
                                {{ $allPercentages['p8Percent'] }}%
                            </th>
                            <th class="white">
                                {{ $allPercentages['p9Percent'] }}%
                            </th>
                            <th class="white">
                                {{ $allPercentages['p10Percent'] }}%
                            </th>
                            <th class="white">
                                100%
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </article>
        </section>
    </main>
</div>
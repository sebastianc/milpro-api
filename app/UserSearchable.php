<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 11/08/2016
 * Time: 10:05
 */

namespace App;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Sofa\Eloquence\Eloquence;

class UserSearchable extends Model{

    protected $table = 'users';
    /**
     * Relations to be loaded by default.
     *
     * @var array
     */
    /*protected $with = [
        'listings'
    ];*/
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    use SoftDeletes;
    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'name' => 25,
        'email' => 35,
    ];


    function toArray(){

        $array = parent::toArray();
        return $array;
    }


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function getEmail(){
        return $this->email;
    }

    /**
     * Calls the user is listing
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function listings()
    {
        return $this->hasMany('App\Call', 'caller')->where('deleted_at', null)->orderBy('created_at', 'desc');
    }

    /**
     * Products the user is listing
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Product', 'seller')->orderBy('created_at', 'desc');
    }

    /**
     * Calculate the rating for a user
     */
    /*public function calculateRating()
    {
        $this->average_rating = Review::where(function ($query) {
                $query->where('seller_id', $this->id)->where('is_seller', 0);
            })->orWhere(function ($query) {
                $query->where('user_id', $this->id)->where('is_seller', 1);
            })->avg('rating');
    }*/
}

<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Sofa\Eloquence\Eloquence;

class Call extends Model
{
    use SoftDeletes;

    protected $with = ['caller'];
    //protected $appends = ['comment_count', 'like_count'];

    /**
     * Custom to array function
     * @return array
     */
    public function toArray(){
        $array = parent::toArray();
        return $array;
    }


    use Eloquence;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = [
        'products' => 23,
        'product_name' => 23,
        'customer' => 23,
        'must' => 23,
        'intend' => 23,
        'like' => 23,
        'caller.name' => 12,
        'caller.email' => 32,
        'ach_value' => 15,
        'enq_value' => 15,
    ];

    function caller()
    {
        return $this->belongsTo('App\UserSearchable', 'caller', 'id');
    }

    function callerUser()
    {
        return $this->caller();
    }

    public function belongsToCurrentUser(){
        if($user = Auth::user()){
            if($user->id == $this->caller){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

}

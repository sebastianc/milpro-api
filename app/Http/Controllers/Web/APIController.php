<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 11/08/2016
 * Time: 14:54
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\API\V1\AuthController;
use App\Http\Controllers\API\V1\ProductController;
use App\Http\Controllers\Controller;

class APIController extends Controller
{
    public function register(Request $request) {
        $controller = new AuthController();
        return $controller->register($request);
    }

    public function getProducts() {
        $controller = new ProductController();
        return $controller->viewAll();
    }
}
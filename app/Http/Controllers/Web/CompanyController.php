<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 02/08/16
 * Time: 17:36
 */

namespace App\Http\Controllers\Web;


use App\UserSearchable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class CompanyController extends BaseController
{
    use AuthenticatesUsers;
    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 600;

    function registerPage(){
        return view('pages.register');
    }

    /**
     * View Companies page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allCompanies(Request $request){
        $user = UserSearchable::find(Auth::user()->id);
        $companies = UserSearchable::where('is_admin', '=', '1')->get();
        foreach($companies as $key => $company) {
            $company['used_acc'] = count(UserSearchable::where('belongs_to', '=', $company['id'])->get());
        }

        return view('pages.companies', compact('companies', 'user'));
    }

    /**
     * Register new company
     * @param Request $request
     * @return mixed
     */
    public function registerCompany(Request $request)
    {
        $data = Input::only('email', 'password', 'name', 'premium', 'premium_expire', 'is_admin', 'area', 'belongs_to', 'accts_nr');

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|min:8',
            'name' => 'required|min:4|max:36|unique:users',
            'premium_expire' => 'required',
            'accts_nr' => 'required'
        ]);
        if ($validator->fails()) {
            //return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
            return redirect()->back()->with('error_message', $validator->errors()->first());
        } else {
            $new_user = new User;
            $data['password'] = bcrypt($data['password']);
            // Only update non-null keys
            foreach ($data as $k => $v) {
                if (!is_null($v)) {
                    $new_user[$k] = $v;
                }
            }
            $new_user['is_admin'] = 1;

            if($new_user->save()) {
                return redirect()->back()->with('success_message', 'Company has been added.');
            }
        }
    }

    /**
     * Edit company
     * @param Request $request
     * @return mixed
     */
    public function editCompany(Request $request, $id)
    {
        $data = Input::only('email', 'name', 'premium', 'premium_expire', 'accts_nr');

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'name' => 'required|min:4|max:36',
            'premium_expire' => 'required',
            'accts_nr' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error_message', $validator->errors()->first());
        } else {
            $user = User::where('id', $id)->first();
            // Only update non-null keys
            foreach ($data as $k => $v) {
                if (!is_null($v)) {
                    $user[$k] = $v;
                }
            }

            if($user->save()) {
                return redirect()->back()->with('success_message', 'Company Edited.');
            }
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 04/08/16
 * Time: 15:59
 */

namespace App\Http\Controllers\Web;


use App\Call;
use App\Product;
use App\UserSearchable;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Namshi\JOSE\SimpleJWS;
//use App\Http\Controllers\API\V1\NotificationController;


class AccountController extends BaseController
{

    /**
     * View Agents page
     * @param $month, $year
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($month, $year){

        $user = UserSearchable::find(Auth::user()->id);
        $agents = UserSearchable::where('belongs_to', '=', Auth::user()->id)->paginate(16);
        $products = Product::where('seller', '=', Auth::user()->id)->get();
        $user->accounts_left = $user->accts_nr - count($agents);

        foreach($agents as $agent) {

            //month to date average

            $calls = Call::where('caller', '=', $agent->id)->where(DB::raw('MONTH(created_at)'), '=', $month)->where(DB::raw('YEAR(created_at)'), '=', $year)->orderBy('created_at', 'desc')->get();
            $numberOfCalls = count($calls);
            $avg = 0;
            $month_orders = 0;
            $month_enquiry = 0;
            foreach ($calls as $call) {
                $call['performance'] = preg_replace('/[^0-9.]+/', '', $call['performance']);
                $avg += $call['performance'];
                $month_orders += preg_replace('/[^0-9.]+/', '', $call['ach_value']);
                $month_enquiry += preg_replace('/[^0-9.]+/', '', $call['enq_value']);
            }

            $agent->month_orders = $month_orders;
            $agent->month_enquiry = $month_enquiry;
            if($numberOfCalls) {
                $avg = $avg / $numberOfCalls;
            }

            $agent->actual= $avg.'%';

            //year to date average

            $callsYear = Call::where('caller', '=', $agent->id)->where(DB::raw('YEAR(created_at)'), '=', $year)->orderBy('created_at', 'desc')->get();
            $numberOfCallsYear = count($callsYear);
            $avgYear = 0;
            $cum_orders = 0;
            $cum_enquiry = 0;
            foreach ($callsYear as $callYear) {
                $callYear['performance'] = preg_replace('/[^0-9.]+/', '', $callYear['performance']);
                $avgYear += $callYear['performance'];
                $cum_orders += preg_replace('/[^0-9.]+/', '', $callYear['ach_value']);
                $cum_enquiry += preg_replace('/[^0-9.]+/', '', $callYear['enq_value']);
            }

            $agent->cum_orders = $cum_orders;
            $agent->cum_enquiry = $cum_enquiry;
            if($numberOfCallsYear) {
                $avgYear = $avgYear / $numberOfCallsYear;
            }
            $agent->ytd_avg= $avgYear.'%';

            //today's stats

            //$callsToday = Call::where('caller', '=', $agent->id)->select(DB::raw('*'))->whereRaw('Date(created_at) = CURDATE()')->get();
            $callsToday = Call::where('caller', '=', $agent->id)->whereRaw('Date(created_at) = CURDATE()')->get();
            $orders = 0;
            $enquiry = 0;
            foreach ($callsToday as $callToday) {
                $callToday['performance'] = preg_replace('/[^0-9.]+/', '', $callToday['performance']);
                $orders += preg_replace('/[^0-9.]+/', '', $callToday['ach_value']);
                $enquiry += preg_replace('/[^0-9.]+/', '', $callToday['enq_value']);
            }

            $agent->orders = $orders;
            $agent->enquiry = $enquiry;

        }

        return view('pages.account', compact('agents', 'products', 'user', 'month', 'year'));
    }

    /**
     * Register new Agent
     * @param Request $request
     * @return mixed
     */
    public function registerAgent(Request $request)
    {
        $user = UserSearchable::find(Auth::user()->id);
        $agents = count(UserSearchable::where('belongs_to', '=', Auth::user()->id)->get());
        if ($agents < $user->accts_nr) {
            try {
                $data = Input::only('email', 'password', 'name', 'premium', 'premium_expire', 'area', 'belongs_to');

                $validator = Validator::make($request->all(), [
                    'email' => 'required|email|unique:users|max:255',
                    'password' => 'required|min:8',
                    'name' => 'required|min:4|max:36|unique:users',
                    'area' => 'required'
                ]);
                if ($validator->fails()) {
                    //return json_encode(['status' => 'failed', 'error' => $validator->errors()->first()]);
                    return redirect()->back()->with('error_message', $validator->errors()->first());
                } else {
                    $new_user = new User;
                    $new_user->email = $data['email'];
                    $new_user->password = bcrypt($data['password']);
                    $new_user->name = $data['name'];
                    $new_user->area = $data['area'];
                    $new_user->belongs_to = Auth::user()->id;

                    if ($new_user->save()) {
                        //return json_encode(['status' => 'success']);
                        return redirect()->back()->with('success_message', 'Agent has been registered.');
                    }

                }
            } catch (\Exception $e) {
                dd($e);
            }
        } else {
            return redirect()->back()->with('error_message', 'You have reached your maximum accounts limit.');
        }
    }

    /**
     * Remove an Agent
     * @param $id
     * @return mixed
     */
    public function removeAgent($id)
    {
        User::destroy($id);
        return redirect()->back()->with('success_message', 'Agent has been removed.');
    }

    /**
     * Add new Product
     * @param Request $request
     * @return mixed
     */
    public function addProduct(Request $request)
    {
        //$user = UserSearchable::find(Auth::user()->id);
        $products = count(Product::where('seller', '=', Auth::user()->id)->get());
        if ($products < 10) {
            try {
                $data = Input::only('name');

                $validator = Validator::make($request->all(), [
                    'name' => 'required|min:4|max:36'
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->with('error_message', $validator->errors()->first());
                } else {
                    $product = new Product;
                    $product->name = $data['name'];
                    $product->seller = Auth::user()->id;

                    if ($product->save()) {
                        return redirect()->back()->with('success_message', 'Product has been added.');
                    }

                }
            } catch (\Exception $e) {
                dd($e);
            }
        } else {
            return redirect()->back()->with('error_message', 'You have reached your maximum products limit.');
        }
    }

    /**
     * Remove a Product
     * @param $id
     * @return mixed
     */
    public function removeProduct($id)
    {
        Product::destroy($id);
        return redirect()->back()->with('success_message', 'Product has been removed.');
    }


    /**
     * Show Product Percentages for an Agent
     * @param $id
     * @return mixed
     */
    public function productPercentages($id)
    {
        $agent = UserSearchable::find($id);
        $agents = UserSearchable::where('belongs_to', '=', Auth::user()->id)->get();
        $user = UserSearchable::find(Auth::user()->id);
        $calls = Call::where('caller', '=', $id)->get();
        $callNumber = count($calls);
        $p1Percent = 0;
        $p2Percent = 0;
        $p3Percent = 0;
        $p4Percent = 0;
        $p5Percent = 0;
        $p6Percent = 0;
        $p7Percent = 0;
        $p8Percent = 0;
        $p9Percent = 0;
        $p10Percent = 0;

        foreach ($calls as $call) {
            $call->p1 = $call->p2 = $call->p3 = $call->p4 = $call->p5 = $call->p6 = $call->p7 = $call->p8 = $call->p9 = $call->p10 = 0;
            $topics = json_decode($call->products,true);
            if (in_array("1", $topics)) {
                $call->p1 = 1;
            }
            if (in_array("2", $topics)) {
                $call->p2 = 1;
            }
            if (in_array("3", $topics)) {
                $call->p3 = 1;
            }
            if (in_array("4", $topics)) {
                $call->p4 = 1;
            }
            if (in_array("5", $topics)) {
                $call->p5 = 1;
            }
            if (in_array("6", $topics)) {
                $call->p6 = 1;
            }
            if (in_array("7", $topics)) {
                $call->p7 = 1;
            }
            if (in_array("8", $topics)) {
                $call->p8 = 1;
            }
            if (in_array("9", $topics)) {
                $call->p9 = 1;
            }
            if (in_array("10", $topics)) {
                $call->p10 = 1;
            }
            $totalProducts = $call->p1 + $call->p2 + $call->p3 + $call->p4 + $call->p5 + $call->p6 + $call->p7 + $call->p8 + $call->p9 + $call->p10;
            $call->percentage = 100/$totalProducts;
            $p1Percent += $call->percentage*$call->p1;
            $p2Percent += $call->percentage*$call->p2;
            $p3Percent += $call->percentage*$call->p3;
            $p4Percent += $call->percentage*$call->p4;
            $p5Percent += $call->percentage*$call->p5;
            $p6Percent += $call->percentage*$call->p6;
            $p7Percent += $call->percentage*$call->p7;
            $p8Percent += $call->percentage*$call->p8;
            $p9Percent += $call->percentage*$call->p9;
            $p10Percent += $call->percentage*$call->p10;
            $call->percentage = number_format($call->percentage,2);
        }
        if($callNumber) {
            $allPercentages['p1Percent'] = number_format($p1Percent / $callNumber, 2);
            $allPercentages['p2Percent'] = number_format($p2Percent / $callNumber, 2);
            $allPercentages['p3Percent'] = number_format($p3Percent / $callNumber, 2);
            $allPercentages['p4Percent'] = number_format($p4Percent / $callNumber, 2);
            $allPercentages['p5Percent'] = number_format($p5Percent / $callNumber, 2);
            $allPercentages['p6Percent'] = number_format($p6Percent / $callNumber, 2);
            $allPercentages['p7Percent'] = number_format($p7Percent / $callNumber, 2);
            $allPercentages['p8Percent'] = number_format($p8Percent / $callNumber, 2);
            $allPercentages['p9Percent'] = number_format($p9Percent / $callNumber, 2);
            $allPercentages['p10Percent'] = number_format($p10Percent / $callNumber, 2);
        } else {
            $allPercentages['p1Percent'] = $allPercentages['p2Percent'] = $allPercentages['p3Percent'] = $allPercentages['p4Percent'] = $allPercentages['p5Percent'] = $allPercentages['p6Percent'] = $allPercentages['p7Percent'] = $allPercentages['p8Percent'] = $allPercentages['p9Percent'] = $allPercentages['p10Percent'] = 0;
        }
        //$products = Product::where('seller', '=', Auth::user()->id)->get();

        return view('pages.percentages', compact('agent', 'calls', 'user', 'allPercentages', 'agents'));
    }



    public function ajaxDispatchSold($id)
    {
        $shoe = Shoe::where('seller', Auth::user()->id)->where('sold', true)->where('id', $id)->first();

        if (!$shoe) {
            return response()->json(['status' => false], 500);
        }

        if ($shoe->purchase->status == 'paid') {
            $shoe->purchase->status = 'dispatched';
            $img = ShoeImage::where('shoe_id', $id)->first();
            $img_url = $img['base_url'].'/'.$img['public_url'];
            $img_url  = str_replace('/original/', '/medium/', $img_url );
            $pushData = [
                'notification_id' => $shoe->purchase->id,
                'notification_type' => 'Dispatched',
                'channel' => 'user_'.$shoe->purchase->buyer_id,
                'recipient' => $shoe->purchase->buyer_id,
                'message' => 'Marked as dispatched',
                'snippet' => $shoe->name,
                'extra_id' => $shoe->id,
                'img' => $img_url,
            ];
            $push = new NotificationController;
            $push->sendNotification($pushData);
        } else {
            $shoe->purchase->status = 'paid';
        }

        $shoe->purchase->save();

        return response()->json(['status' => true, 'shoe_status' => $shoe->purchase->status]);
    }

    public function postReview(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shoe_id' => 'required',
            'rating' => 'required|numeric|between:0,5',
            'text' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error_message', $validator->errors()->first());
        }

        $id = $request->input('shoe_id');
        $shoe = Shoe::findOrFail($id);
        $isSeller = Auth::user()->id == $shoe->sellerUser->id;

        if (Review::where('shoe_id', $id)->where('is_seller', $isSeller)->count() != 0) {
            return redirect()->back()->with('error_message', 'You have already reviewed this transaction.');
        }

        $review = new Review();
        $review->user_id = $shoe->purchase->user->id;
        $review->seller_id = $shoe->sellerUser->id;
        $review->rating = $request->input('rating');
        $review->text = $request->input('text');
        $review->is_seller = $isSeller;
        $review->shoe_id = $shoe->id;
        $review->save();

        $user = UserSearchable::find($isSeller ? $review->user_id : $review->seller_id);
        $user->calculateRating();
        $user->save();

        $pushData = [
            'notification_id' => $review->id,
            'notification_type' => 'Review',
            'channel' => 'user_' . ($isSeller ? $review->user_id : $review->seller_id),
            'recipient' => ($isSeller ? $review->user_id : $review->seller_id),
            'message' => 'Someone reviewed your transaction!',
            'snippet' => substr($review->text, 0, 100),
            'extra_id' => $review->id,

        ];
        $push = new NotificationController;
        $push->sendNotification($pushData);
        
        return redirect()->back()->with('success_message', 'Your review has been saved.');
    }

    public function addUsername(Request $request)
    {
        $user = Auth::user();
        $validation = Validator::make($request->all(), ['username' => 'required|unique:users|min:6']);
        if ($validation->fails()) {
            return redirect('/account/complete')->with('error_message', $validation->messages()->first());
        } else {
            $user->username = $request->username;
            $user->save();
            return redirect('/account');
        }
    }

}
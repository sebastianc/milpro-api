<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 02/08/16
 * Time: 17:36
 */

namespace App\Http\Controllers\Web;


use App\UserSearchable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class AuthController extends BaseController
{
    //use ThrottlesLogins, AuthenticatesUsers;
    use AuthenticatesUsers;
    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 600;

    /**
     * Register new user
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
       try{
           $data = Input::only('email', 'password', 'name', 'premium', 'premium_expire', 'is_admin', 'area', 'belongs_to', 'accts_nr');


           $validator = Validator::make($request->all(), [
               'email' => 'required|email|unique:users|max:255',
               'password' => 'required|min:8',
               'name' => 'required|min:4|max:36|unique:users',
               'premium' => 'required'
           ]);
           if ($validator->fails()) {
               //return json_encode(['status' => 'failed', 'error' => $validator->errors()->first()]);
               return redirect()->back()->with('error_message', $validator->errors()->first());
           } else {
               $new_user = new User;
               $new_user->email = $data['email'];
               $new_user->password = bcrypt($data['password']);
               $new_user->name = $data['name'];
               if(!empty($data['premium'])) {
                   $new_user->premium = $data['premium'];
               }
               $new_user->premium_expire = $data['premium_expire'];
               $new_user->is_admin = (bool)$data['is_admin'];
               $new_user->area = $data['area'];
               $new_user->belongs_to = (int)$data['belongs_to'];
               $new_user->accts_nr = (int)$data['accts_nr'];

               if($new_user->save()) {
                   Auth::login($new_user);
                   return json_encode(['status' => 'success']);
               }

           }
       }catch (\Exception $e){
           dd($e);
       }
    }

    function registerPage(){
        return view('pages.register');
    }

    /**
     * Login user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login( Request $request)
    {
        // grab credentials from the request
        $credentials = Input::only('email', 'password');

        try {
            //If too many login attempts block IP
            if ($this->hasTooManyLoginAttempts($request)) {
                return redirect('/dashboard/login')->with('error_message' , 'Too many failed login attempts');
            }
            // attempt to verify the credentials for the user
            if (!Auth::attempt($credentials)) {
                $user = User::where('email', $credentials['email'])->where('parse_id', '!=', 'null')->where('initial_pass_reset', 0)->first();
                if($user){
                    return redirect('/dashboard/login')->with(['parse_user_login' => true , 'error_message' => 'Please reset your password.']);
                }
                //Failed so increment attempts
                $this->incrementLoginAttempts($request);
                return redirect('/dashboard/login')->with('error_message' , 'Incorrect email or password')->with('attempts_remaining' , $this->retriesLeft($request));
            }
        } catch (\Exception $e) {
            // something went wrong whilst attempting to login user
            return redirect('/dashboard/login')->with('error_message' , 'Something went wrong, please try again later.');
//            dd($e);

        }
        // all good so return the user and reset login attempts
        $this->clearLoginAttempts($request);
        $user = UserSearchable::find(Auth::user()->id);
        if ($user->is_owner) {
            return redirect()->intended('dashboard/companies/')->with('user', $user);
        } elseif ($user->is_admin) {
            if(strtotime($user->premium_expire)>strtotime(date('d-m-Y'))) {
                return redirect()->intended('dashboard/account/' . date('n') . '/' . date('Y'))->with('user', $user);
            } else {
                return redirect()->back()->with('error_message', 'Your premium has expired!');
            }
        } else {
            //Failed so increment attempts
            $this->incrementLoginAttempts($request);
            return redirect('dashboard/login')->with('error_message' , 'Incorrect email or password')->with('attempts_remaining' , $this->retriesLeft($request));
        }

    }

    /**
     * View login page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loginPage(Request $request){
        return view('pages.login');
    }

    /**
     * Logout user
     * @return mixed
     */
    public function logout(Request $request)
    {
        Auth::logout();
        //Session::flush();
        $request->session()->flush();
        return redirect('/dashboard/');

    }

}
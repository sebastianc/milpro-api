<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 24/02/16
 * Time: 16:35
 */

namespace App\Http\Controllers\API\V1;

use App\Product;
use App\UserSearchable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends ApiController
{
    function viewAll(){

        $products = Product
            ::orderBy('created_at', 'desc')
            ->paginate(10);

        return parent::api_response($products, true, ['return' => 'all products'], 200);
    }

    function get($id){
        $product = Product::find($id);
        if($product) {
            return parent::api_response($product, true, ['return' => 'selected product'], 200);
        } else {
            return parent::api_response([], false, ['error' => 'Product not found'], 404);
        }
    }

    /**
     * Add new product
     * @param Request $request
     * @return mixed
     */
    function add(Request $request)
    {
        $data = Input::only('name');
        $user = UserSearchable::find(Auth::user()->id);
        $products = count(Product::where('seller', '=', Auth::user()->id)->get());

        if($user->belongs_to==0 && $products<10){
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:4|max:36'
            ]);
            if ($validator->fails()) {
                return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
            } else {
                $product = new Product;
                $product->name = $data['name'];
                $product->seller = Auth::user()->id;

                if ($product->save()) {
                    return $this->api_response([], true, ['Success' => 'Product added'], 200);
                }
            }
        } else {
            return parent::api_response([], false, ['Error' => 'You are not allowed to add products or you exceeded the limit of maximum 10'], 200);
        }

    }

}
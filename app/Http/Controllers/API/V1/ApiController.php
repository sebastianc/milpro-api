<?php

/**
 * Created by PhpStorm.
 * User: seb
 * Date: 18/01/16
 * Time: 16:29
 */

namespace App\Http\Controllers\API\V1;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{
    /**
     * API response function to be used by all endpoints
     * @param array $body
     * @param null $message
     * @param bool $success
     * @param int $status
     * @return json response
     */
    public function api_response($body, $success = true, $message = null, $status = 200){
        if(isset($body['data'])){
            $count = count($body['data']);
        }else{
            $count = count($body);
            $body = ['data' => $body];
        }
        $payload = [
            'result' => [
                'success' => $success,
                'message' => $message,
                'count' => $count,],
            'response_payload' => $body
        ];
        return Response::json($payload, $status);
    }
}
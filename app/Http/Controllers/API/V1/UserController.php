<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 25/02/16
 * Time: 10:13
 */

namespace App\Http\Controllers\API\V1;


use App\Product;
use App\Call;
use App\UserSearchable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class UserController extends ApiController
{

    /*public function getProducts($id) {

        $products = Product::orderBy('created_at', 'desc')->orderBy('name', 'asc')->paginate(10);

        return parent::api_response($products, true, ['return' => 'products by user with id '.$id], 200);
    }*/

    public function getUsers() {
        $users = UserSearchable
            ::orderBy('is_owner', 'desc')->orderBy('is_admin', 'desc')->orderBy('created_at', 'desc')->orderBy('name', 'asc')->paginate(15);
        return parent::api_response($users, true, ['return' => 'all users']);
    }

    function getById($id){
        //$user = UserSearchable::with('follows', 'followers')->find($id);
        $user = UserSearchable::with('listings')->find($id);
        if($user){
            return parent::api_response($user, true, ['return' => 'user '], 200);
        } else {
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    public function getMonthly($id){
        $info = Input::only('months');
        $monthsAgo = Carbon::today()->subMonths($info['months']);
        //$calls = Call::where('caller', '=', $id)->orderBy('created_at', 'desc')->get();
        $calls = Call::where('caller', '=', $id)->where( DB::raw('date(created_at)'), '>', $monthsAgo )->orderBy('created_at', 'desc')->get();
        $numberOfCalls = count($calls);
        $avg = 0;
        foreach ($calls as $call) {
            $call['performance'] = preg_replace('/[^0-9.]+/', '', $call['performance']);
            $avg += $call['performance'];
        }

        $avg = $avg / $numberOfCalls;

        return parent::api_response($avg.'%', true, ['return' => 'Performance percentage for user for the last '.$info['months'].' months'], 200);
    }

    public function getWeekly($id){
        $info = Input::only('weeks');
        $weeksAgo = Carbon::today()->subWeeks($info['weeks']);
        $calls = Call::where('caller', '=', $id)->where( DB::raw('date(created_at)'), '>', $weeksAgo )->orderBy('created_at', 'desc')->get();
        $numberOfCalls = count($calls);
        $avg = 0;
        foreach ($calls as $call) {
            $call['performance'] = preg_replace('/[^0-9.]+/', '', $call['performance']);
            $avg += $call['performance'];
        }

        $avg = $avg / $numberOfCalls;

        return parent::api_response($avg.'%', true, ['return' => 'Performance percentage for user for the last '.$info['weeks'].' weeks'], 200);
    }

    public function getDaily($id){

        $calls = DB::table('calls')->select(DB::raw('*'))
            ->whereRaw('Date(created_at) = CURDATE()')
            ->where('caller', '=', $id)
            ->orderBy('created_at', 'desc')
            ->get();
        $numberOfCalls = count($calls);
        $avg = 0;
        foreach ($calls as $call) {
            $call->performance = preg_replace('/[^0-9.]+/', '', $call->performance);
            $avg += $call->performance;
        }

        $avg = $avg / $numberOfCalls;

        return parent::api_response($avg.'%', true, ['return' => 'Performance percentage for user for today '], 200);
    }

    public function search($term){
        //$users = UserSearchable::where('id', '!=', Auth::user()->id)->with('following')->search($term)->paginate(15);
        //return parent::api_response($users, true, ['return' => 'search for '.$term], 200);
    }

}
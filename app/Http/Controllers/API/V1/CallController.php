<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 24/02/16
 * Time: 16:35
 */

namespace App\Http\Controllers\API\V1;

use App\Call;
use App\Product;
use App\UserSearchable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CallController extends ApiController
{
    function viewAll(){

        $calls = Call
            ::with('caller')
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return parent::api_response($calls, true, ['return' => 'all calls'], 200);
    }

    function get($id){
        $call = Call::find($id);
        if($call) {
            $productIds = json_decode($call['products'],true);
            $products = array();
            foreach($productIds as $prodId) {
                $products[] = Product::find($prodId);
            }
            $call['products'] = $products;
            return parent::api_response($call, true, ['return' => 'selected call'], 200);
        } else {
            return parent::api_response([], false, ['error' => 'Call not found'], 404);
        }
    }

    public function search(){
        $filters = Input::only('term', 'caller');
        $sort = Input::get('sort');

        $calls = Call::where('seller', '!=' , Auth::user()->id)->with('type_info', 'genders', 'countries', 'categ_info', 'subcateg_info', 'sizes', 'comments', 'images', 'seller', 'likedBy');
            if (isset($filters['caller'])) {
                $calls->whereHas('caller', function ($query) use ($filters) {
                    if (isset($filters['caller'])) {
                        $query->whereIn('id', json_decode($filters['caller'], true));
                    }
                });
            }

        if(isset($filters['term'])){
            $calls->search($filters['term']);
        }

        if (empty($sort)) {
            $sort = 'recent';
        }

        switch ($sort){
            case 'price_low_high':
                if(!$filters['term']){
                    $calls->orderBy('cost', 'asc');
                }else{
                    $query = $calls->getQuery();
                    $query->orders = array_prepend($query->orders, ['column' => 'cost', 'direction' => 'ASC']);
                    $calls = $calls->setQuery($query);
                }

                break;
            case 'price_high_low':
                if(!$filters['term']){
                    $calls->orderBy('cost', 'desc');
                }else{
                    $query = $calls->getQuery();
                    $query->orders = array_prepend($query->orders, ['column' => 'cost', 'direction' => 'DESC']);
                    $calls = $calls->setQuery($query);
                }
                break;
            case 'recent':
                if(!$filters['term']){
                    $calls->orderBy('created_at', 'desc');
                }else{
                    $query = $calls->getQuery();
                    $query->orders = array_prepend($query->orders, ['column' => 'created_at', 'direction' => 'DESC']);
                    $calls = $calls->setQuery($query);
                }
                break;
        }

        return parent::api_response($calls->paginate(10), true, ['return' => 'Search Results '.$filters['term']], 200);
    }

}
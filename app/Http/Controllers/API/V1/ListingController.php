<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 26/02/16
 * Time: 12:16
 */

namespace App\Http\Controllers\API\V1;

use App\Call;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class ListingController extends ApiController
{

    protected $uploadsFolder = 'uploads/users/';
    protected $imageCacheRoute = 'images/original/users/';


    public function viewAll () {
        $calls = Call::with('caller')->orderBy('created_at', 'desc')->where('caller', Auth::user()->id)->paginate(10);
        return parent::api_response($calls, true, ['success' => 'Users Listings'], 200);
    }

    protected function upload(){
        $info = Input::only('customer', 'ach_value', 'enq_value', 'product_name', 'm_n_a', 'm_p_a', 'm_f_a', 'i_p_a', 'i_f_a', 'l_p_a', 'l_f_a', 'must', 'intend', 'like', 'products');

        $call = new Call;
        $call->caller = Auth::user()->id;

        //TODO: Performance calculations

        unset($info['m_n_a'], $info['m_p_a'], $info['m_f_a'], $info['i_p_a'], $info['i_f_a'], $info['l_p_a'], $info['l_f_a']);

        $call->performance = '65%';
        foreach($info as $field => $val){
            $call->$field = $val;
        }
        try{
            $call->save();

            return parent::api_response($call, true, ['success' => 'call saved'], 200);
        }catch (QueryException $e){
            return parent::api_response($e->errorInfo, false, ['error' => 'call save failed'], 500);
        }
    }

    protected function edit(){

        $info = Input::only('id', 'customer', 'ach_value', 'enq_value', 'product_name', 'performance', 'must', 'intend', 'like', 'products');

        $call = Call::find($info['id']);
        foreach($info as $field => $val){
            $call->$field = $val;
        }
        try{
            $call->save();

            return parent::api_response($call, true, ['success' => 'call edited'], 200);
        }catch (QueryException $e){
            return parent::api_response($e->errorInfo, false, ['error' => 'call editing failed'], 500);
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 18/01/16
 * Time: 16:51
 */

namespace App\Http\Controllers\API\V1;

use App\User;
use App\Call;
use App\Product;
use App\UserSearchable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;


class AuthController extends ApiController

{
    use AuthenticatesUsers;

    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 600;


    /**
     * Log in a user.
     *
     * @param Request $request
     * @return json
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        //Limit the amount of times users can login
        if ($this->hasTooManyLoginAttempts($request)) {
            return parent::api_response([], 'too many authentication attempts, try again in '.$this->lockoutTime.' seconds', false, 401);
        }

        $token = JWTAuth::attempt($credentials);

        //If the login attempt failed
        if (!$token) {
            $this->incrementLoginAttempts($request);
            return parent::api_response([], 'invalid credentials', 401);
        }

        $user = User::findOrFail(Auth::user()->id);

        if ($user->is_owner || $user->is_admin || !$user->belongs_to) {
            return parent::api_response([], 'invalid credentials, use web panel to log in', 401);
        }

        $company = User::findOrFail($user->belongs_to);

        if(strtotime($company->premium_expire)<strtotime(date('d-m-Y'))) {
            return parent::api_response([], "Your company's premium has expired!", 401);
        }

        return parent::api_response([
            'token' => $token,
            'user' => $user
        ]);
    }

    /**
     * Register new user
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
        $data = Input::only('email', 'password', 'name', 'premium', 'premium_expire', 'is_admin', 'area', 'belongs_to', 'accts_nr');

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|min:8',
            'name' => 'required|min:4|max:36|unique:users',
            'premium' => 'required'
        ]);
        if ($validator->fails()) {
            return parent::api_response([], false, ['error' => $validator->errors()->first()], 401);
        } else {
            $new_user = new User;
            $new_user->email = $data['email'];
            $new_user->password = bcrypt($data['password']);
            $new_user->name = $data['name'];
            if(!empty($data['premium'])) {
                $new_user->premium = $data['premium'];
            }
            $new_user->premium_expire = $data['premium_expire'];
            $new_user->is_admin = (bool)$data['is_admin'];
            $new_user->area = $data['area'];
            $new_user->belongs_to = (int)$data['belongs_to'];
            $new_user->accts_nr = (int)$data['accts_nr'];

            if($new_user->save()) {
                $request = new Request();
                $request->merge($data);
                return $this->login($request);
            }
        }
    }

    /**
     * Logout user
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request){
        if(!Auth::logout()){
            $data = Input::only('token');
            $validator = Validator::make($request->all(), [
                'token' => 'required|max:355'
            ]);
            if (!$validator->fails()) {
                if ($data['token']) {
                    JWTAuth::invalidate($data['token']);
                }
            } else {
                return parent::api_response([], false, ['Error' => 'user not found'], 200);
            }
            return $this->api_response([], true, ['Success' => 'logged out'], 200);
        }
    }

    /**
     * Reset password
     * @return mixed
     */
    public function reset(){
        $email = stripslashes(Input::get('email'));
        $user = User::where('email', $email)->first();

        if($user){
            $raw_pass = str_random(12);
            $new_pass = bcrypt($raw_pass);
            $user->password = $new_pass;
            $data['user'] = $user->toArray();
            $data['password'] = $raw_pass;
            if($user->parse_id && !$user->initial_pass_reset){
                $user->initial_pass_reset = 1;
            }
            if($user->save()){
                Mail::send('emails.API.V1.reset', ['data' => $data], function ($m) use ($email, $data) {
                    $m->from('hey@Milpro-test.com', 'Milpro');

                    $m->to($email, 'user')->subject('Pass Reset');
                });
                return parent::api_response([], true, ['Success' => 'new pass sent'], 200);
            }

        }else{
            return parent::api_response([], false, ['Error' => 'user not found'], 404);
        }

    }

    public function appdata () {
        $calls = Call::orderby('customer')->orderby('product_name')->get();
        $products = Product::all();
        $users = UserSearchable::orderby('name')->get();
        $admins = UserSearchable::orderby('name')->where('is_admin', 1)->get();
        $owner = UserSearchable::orderby('name')->where('is_owner', 1)->get();

        $data = [
            'calls' => $calls,
            'products' => $products,
            'users' => $users,
            'admins' => $admins,
            'owner' => $owner
        ];

        return parent::api_response($data, true, ['Success' => 'Appdata'], 200);
    }

    public function update(Request $request)
    {
        $app = $request->input('app');
        $appVersion = config('milpro.version.'. $app);

        if ($appVersion == null) {
            return parent::api_response([], false, ['error' => 'invalid app parameter'], 200);
        }

        $shouldUpdate = version_compare($appVersion, $request->input('version'), '>');

        $data = [
            'should_update' => $shouldUpdate
        ];

        return parent::api_response($data, true, ['Success' => 'update'], 200);
    }


}
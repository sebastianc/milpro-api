<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellerToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('seller')->unsigned();
            $table->foreign('seller')->references('id')->on('users');
        });
        Schema::table('products', function (Blueprint $table) {
            $sql="INSERT INTO `products` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`, `seller`) VALUES
	(1, 'Prod 1', NULL, NULL, NULL, 3),
	(2, 'Prod 2', NULL, NULL, NULL, 3),
	(3, 'Prod 3', NULL, NULL, '2016-11-09 14:20:48', 3),
	(4, 'Prod 4', NULL, NULL, NULL, 3);";
            DB::statement($sql);
        });
        Schema::table('calls', function (Blueprint $table) {
            $sql="INSERT INTO `calls` (`id`, `customer`, `ach_value`, `enq_value`, `product_name`, `performance`, `must`, `intend`, `like`, `caller`, `products`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'Ze Customer', '1000£', '2000', 'Selling good stuff', '65%', '2 per day', '10 per day', '100 per day', 4, '{\"a\":1,\"b\":3}', NULL, '2016-11-03 13:12:53', '2016-11-03 13:12:53'),
	(2, 'Ze 2', '1000£', '2000', 'Selling good stuff', '75%', '2 per day', '10 per day', '100 per day', 8, '{\"a\":1, \"b\":4, \"c\":5}', NULL, '2016-11-03 13:12:53', '2016-11-03 13:12:53'),
	(3, 'Ze 2', '1000£', '2000', 'Selling good stuff', '95%', '2 per day', '10 per day', '100 per day', 8, '{\"a\":1,\"b\":3}', NULL, '2016-11-03 13:12:53', '2016-10-03 13:12:53'),
	(4, 'Ze Customer', '150£', '500', 'Selling good stuff', '5%', '2 per day', '10 per day', '100 per day', 4, '{\"a\":2,\"b\":5}', NULL, '2016-11-07 13:12:53', '2016-11-07 13:12:53');";
            DB::statement($sql);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}

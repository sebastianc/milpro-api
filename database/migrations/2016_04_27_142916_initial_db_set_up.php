<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDbSetUp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('push_token')->nullable();
            $table->enum('premium', array('M', 'Y'))->nullable();
            $table->timestamp('premium_expire')->nullable();
            $table->boolean('is_admin')->default(0);
            $table->boolean('is_owner')->default(0);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('verification_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('code');
            $table->timestamps();
        });
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->string('ip');
            $table->boolean('used')->default(0);
            $table->timestamps();
        });
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->text('content');
            $table->string('image');
            $table->timestamps();
        });
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('calls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer');
            $table->string('ach_value');
            $table->string('enq_value');
            $table->string('product_name');
            $table->string('performance');
            $table->longText('must');
            $table->longText('intend');
            $table->longText('like');
            $table->integer('caller')->unsigned();
            $table->foreign('caller')->references('id')->on('users')->onDelete('cascade');
            $table->string('products');
            //$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
        Schema::drop('password_resets');
        Schema::drop('verification_codes');
        Schema::drop('users');
        Schema::drop('calls');
        Schema::dropIfExists('product_categories');
    }
}

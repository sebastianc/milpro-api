<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('belongs_to')->default(0);
            $table->integer('accts_nr')->default(0);
            $table->string('area')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $sql="INSERT INTO `users` (`id`, `name`, `email`, `password`, `push_token`, `premium`, `premium_expire`, `is_admin`, `is_owner`, `remember_token`, `deleted_at`, `created_at`, `updated_at`, `belongs_to`, `accts_nr`, `area`) VALUES
	(1, 'Owner Guy', 'user@user.com', '$2y$10$931lAKbPzBViLI2/z250T.hbHdG859fO26/C9QA97jTfZBAiLweKy', NULL, NULL, NULL, 0, 1, 'Cs1SRiRTtlNXlJvqJ8OmmGLqVjGkryM213aAJa1xReav3bAadxVhTvVBDBiC', NULL, '2016-11-02 09:48:50', '2016-11-07 14:43:55', 0, 0, NULL),
	(3, 'Test Admin', 'explorehumanity@gmail.com', '$2y$10$931lAKbPzBViLI2/z250T.hbHdG859fO26/C9QA97jTfZBAiLweKy', NULL, NULL, '2017-11-09 17:19:37', 1, 0, 'PF2kwtrhSYOcmjVNZZYjaPMPadByauWqo96socVfjJTJOQq7dYBYykce3Fag', NULL, '2016-11-02 10:51:08', '2016-11-10 10:38:24', 0, 5, NULL),
	(4, 'Test User2', 'asd@email.com', '$2y$10$931lAKbPzBViLI2/z250T.hbHdG859fO26/C9QA97jTfZBAiLweKy', NULL, 'Y', '2016-11-04 14:12:32', 0, 0, NULL, NULL, '2016-11-03 10:43:30', '2016-11-03 10:43:30', 3, 10, 'North'),
	(8, 'Test User3', 'asd2@email.com', '$2y$10$931lAKbPzBViLI2/z250T.hbHdG859fO26/C9QA97jTfZBAiLweKy', NULL, 'Y', '2016-11-03 11:04:56', 0, 0, NULL, NULL, '2016-11-03 11:08:57', '2016-11-03 11:08:57', 3, 20, 'West'),
	(9, 'Test User5', 'asd3@email.com', '$2y$10$931lAKbPzBViLI2/z250T.hbHdG859fO26/C9QA97jTfZBAiLweKy', NULL, 'Y', '2016-11-03 00:00:00', 1, 0, NULL, NULL, '2016-11-03 11:08:57', '2016-11-09 17:27:47', 0, 20, 'West'),
	(11, 'Test 1919', 'use222r@user.com', '$2y$10$931lAKbPzBViLI2/z250T.hbHdG859fO26/C9QA97jTfZBAiLweKy', NULL, 'Y', '2016-11-14 00:00:00', 1, 0, NULL, NULL, '2016-11-09 17:28:19', '2016-11-09 17:28:19', 0, 22, NULL);";
            DB::statement($sql);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}

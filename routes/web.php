<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['prefix' => 'dashboard'], function ($request) {

    Route::post('/register', 'Web\AuthController@register');
    Route::get('/register', 'Web\AuthController@registerPage');
    Route::post('/login', 'Web\AuthController@login');
    Route::get('/', 'Web\AuthController@loginPage');
    Route::get('/login', 'Web\AuthController@loginPage');
    Route::get('/logout', 'Web\AuthController@logout');

    /*Route::get('/', 'Web\SearchController@explore');
    Route::get('/wanted', 'Web\SearchController@wanted');
    Route::get('/search', 'Web\SearchController@searchRedirect');
    Route::post('/search/filter', 'Web\SearchController@ajax_search');
    Route::get('/search/users/{term}', 'Web\SearchController@searchUsers');
    Route::get('/search/{term}', 'Web\SearchController@search');*/

    //Must be logged in to access
    Route::group(['middleware' => 'auth'], function($request) {

        Route::group(['prefix' => 'companies'], function ($request) {
            Route::get('/', 'Web\CompanyController@allCompanies');
            Route::post('/add', 'Web\CompanyController@registerCompany');
            Route::post('/edit/{id}', 'Web\CompanyController@editCompany');
            //Route::post('/delete/{id}', 'Web\CompanyController@ajaxDelete');
        });

        Route::group(['prefix' => 'account'], function ($request) {
            Route::get('/percentages/{id}', 'Web\AccountController@productPercentages');
            Route::get('/{month}/{year}', 'Web\AccountController@index');
            Route::post('/register', 'Web\AccountController@registerAgent');
            Route::post('/remove/{id}', 'Web\AccountController@removeAgent');
            Route::post('/add-prod', 'Web\AccountController@addProduct');
            Route::post('/remove-prod/{id}', 'Web\AccountController@removeProduct');

        });

    });

});

Route::any( '{catchall}', function ( $page ) {
    return view('pages.404');
} )->where('catchall', '(.*)');
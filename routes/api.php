<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function($request) {
    //Account management
    Route::group(['prefix' => 'auth'], function ($request) {
        Route::post('/login', 'API\V1\AuthController@login');
        Route::post('/register', 'API\V1\AuthController@register');
        Route::get ('/logout', 'API\V1\AuthController@logout');
        Route::post('/reset', 'API\V1\AuthController@reset');
        Route::get ('/appdata', 'API\V1\AuthController@appdata');
    });

    //AUTHENTICATED ROUTES
    Route::group(['middleware' => 'jwt'], function ($request) {

        //Calls
        Route::group(['prefix' => 'calls'], function ($request) {
            Route::get('/', 'API\V1\CallController@viewAll');
            Route::get('/{product_id}', 'API\V1\CallController@get');
        });

        //Listings
        Route::group(['prefix' => 'listings'], function ($request) {
            Route::post('new', 'API\V1\ListingController@upload');
        });

        //Products
        Route::group(['prefix' => 'products'], function ($request) {
            Route::get('/', 'API\V1\ProductController@viewAll');
            Route::get('/{product_id}', 'API\V1\ProductController@get');
            Route::post('/add', 'API\V1\ProductController@add');
        });

        //Users
        Route::group(['prefix' => 'users'], function ($request) {
            Route::get('/', 'API\V1\UserController@getUsers');
            Route::get('/{id}/', 'API\V1\UserController@getById');
            Route::get('/monthly-stats/{id}/', 'API\V1\UserController@getMonthly');
            Route::get('/weekly-stats/{id}/', 'API\V1\UserController@getWeekly');
            Route::get('/daily-stats/{id}/', 'API\V1\UserController@getDaily');
        });

    });

});